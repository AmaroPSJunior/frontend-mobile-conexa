# Instruções para executar projeto

dependências:

1 - Ter instalado aplicativo "Expo", no Android ou IOS para rodar o projeto.

https://expo.dev/client 

<br/>

2 - Navegue ate o diretório conexa_agendamento e execute os comandos abaixo:

para instalar as dependências de projeto
````
yarn
````

3 - para executar o teste
````
expo start
````

4 - faça o login no aplicativo "Expo", e sincronize o qr code.