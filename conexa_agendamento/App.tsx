import React from 'react';
import AppLoader from 'expo-app-loading';
import { StatusBar } from 'expo-status-bar';
import { DMSerifDisplay_400Regular } from '@expo-google-fonts/dm-serif-display';
import { useFonts, DMSans_400Regular } from '@expo-google-fonts/dm-sans';
import { ThemeProvider } from 'styled-components/native';

import theme from './src/theme';
import { AuthProvider } from '@hooks/auth';
import { Routes } from './src/routes';

export default function App() {
  const [fontsLoaded] = useFonts({
    DMSerifDisplay_400Regular,
    DMSans_400Regular,
  })

  const style = {
      backgroundColor: 'red',    
  }

  if(!fontsLoaded) return (<AppLoader />);
  return (
    <ThemeProvider theme={theme}>
      <StatusBar style="light" translucent backgroundColor='transparent' />
      <AuthProvider>     
        <Routes />
      </AuthProvider>
    </ThemeProvider>
  );
}

