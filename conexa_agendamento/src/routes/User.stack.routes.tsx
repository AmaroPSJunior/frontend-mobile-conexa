import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { Home } from '@screens/Home'
import { SignIn } from '@screens/Signin'
import { Schedule } from '@screens/Schedule';
import { NewSchedule } from '@screens/NewSchedule';

const { Navigator, Screen,  } = createNativeStackNavigator();

export function UserStackRoutes() {
    return(
        <Navigator screenOptions={{ headerShown: false }}>
            <Screen name="signin" component={SignIn} />
            <Screen name="home" component={Home} />
            <Screen name="schedule" component={Schedule} />
            <Screen name="newschedule" component={NewSchedule} />
        </Navigator>
    )
}