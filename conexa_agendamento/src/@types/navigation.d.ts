export type ScheduleNavigationProps = {
    id: string | number;
}

export declare global {
    namespace ReactNavigation {
        interface RootParamList {
            home: undefined;
            signin: undefined;
            schedule: ScheduleNavigationProps;
        }
    }
}
