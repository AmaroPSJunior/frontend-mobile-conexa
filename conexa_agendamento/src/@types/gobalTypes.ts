
export interface ISchedule {
    dataConsulta: string;
    id: number;
    medico: {
        email: string;
        id: number;
        nome: string;
    };
    observacao: string;
    paciente: string;
} | Object;
