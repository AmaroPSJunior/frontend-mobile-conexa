import styled, { css } from 'styled-components/native';


export const Container = styled.View`
    flex: 1;
    justify-content: center;
    height: auto;
    padding: 0;
`;

export const Title = styled.Text`
    font-size: 14px;
    ${({ theme }) => css`
        color: ${theme.COLORS.TITLE};
        font-family: ${theme.FONTS.TEXT};
    `};
`;

export const Card = styled.View`
    margin: 10px 0;
    padding: 20px;
    width: 100%;
    border-radius: 12px;

    ${({ theme }) => css`
        font-family: ${theme.FONTS.TITLE};
        background-color: ${theme.COLORS.BACKGROUND};
    `};
`;

export const Row = styled.View`
    flex-direction: row;
    justify-content: space-between;
`;

export const Text = styled.Text`

    ${({ theme }) => css`
        color: ${theme.COLORS.PRIMARY_800};
        font-family: ${theme.FONTS.TITLE};
    `};
`;

export const Load = styled.ActivityIndicator.attrs(({ theme }) => ({
    color: theme.COLORS.TITLE
}))``;