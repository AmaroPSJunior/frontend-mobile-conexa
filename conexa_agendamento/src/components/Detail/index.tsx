import React from 'react';
import { Container, Card, Row, Text } from './styles';


export function Detail({ schedule }) {
    return (
        <Container style={{height: '100%'}}>
            <Card>
                <Row>
                    <Text>Paciente:</Text>
                    <Text>{ schedule.paciente }</Text>
                </Row>
                <Row>
                    <Text>Médico:</Text>
                    <Text>{ schedule && schedule.medico ? schedule.medico.nome : ''}</Text>
                </Row>
                <Row>
                    <Text>Observação:</Text>
                    <Text>{ schedule.observacao }</Text>
                </Row>
                <Row>
                    <Text>Data:</Text>
                    <Text>{ schedule.dataConsulta }</Text>
                </Row>
            </Card>
        </Container>
    )
}