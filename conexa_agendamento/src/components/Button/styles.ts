import styled, { css } from 'styled-components/native';
import { TouchableHighlight } from 'react-native';

export type TypeProps = 'primary' | 'secondary';

type ContainerProps = {
    type: TypeProps;
}

export const Container = styled(TouchableHighlight) <ContainerProps>`
    flex: 1;
    min-height: 55px;
    max-height: 55px;
    border-radius: 12px;
    justify-content: center;
    align-items: center;
    background-color: ${({ theme, type}) => type === 'primary' ? theme.COLORS.SUCCESS_900 : theme.COLORS.PRIMARY_800};
    margin-bottom: 20px;
`;

export const Title = styled.Text`
    font-size: 14px;
    ${({ theme }) => css`
        color: ${theme.COLORS.TITLE};
        font-family: ${theme.FONTS.TEXT};
    `};
`;

export const Load = styled.ActivityIndicator.attrs(({ theme }) => ({
    color: theme.COLORS.TITLE
}))``;