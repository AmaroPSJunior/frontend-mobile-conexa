import React from 'react';
import { TouchableHighlightProps } from 'react-native';
import { Container, Load, Title, TypeProps } from './styles';

type Props = TouchableHighlightProps & {
    type?: TypeProps;
    title: string;
    isLoading?: boolean;
}

export function Button({ title, type = 'primary', isLoading = false, ...rest }: Props) {
    return <Container type={type} disabled={isLoading} { ...rest }>
        { isLoading ? <Load /> : <Title>{title}</Title> }
    </Container>
}