import React, { useEffect, useState } from 'react';
import { ModalProps } from 'react-native';
import { Button } from '@components/Button';
import { 
    Container,
    Content,
    Card,
    TypeProps,
    Text,
    View,
    Title,
    Footer,
    ContainerModal,
} from './styles';

type Props = ModalProps & {
    title?: string;
    description?: string;
    textBtnCancel?: string;
    textBtnConfirm?: string;
    btnConfirm?: boolean;
    transparent?: boolean;
    typeBtn1?: TypeProps;
    typeBtn2?: TypeProps;
    animationType?:string;
    onConfirm: Function;
    onCancel: Function;
    open: boolean;
}

export function Modal({
        title = 'Tem certeza que deseja confirmar?',
        description = '',
        btnConfirm = false,
        textBtnCancel = 'Não',
        textBtnConfirm = 'Sim',
        typeBtn1 = 'secondary',
        typeBtn2 = 'secondary',
        onConfirm,
        onCancel,
        open,
        animationType = "fade",
        transparent = true,
        ...rest 
    }: Props) {
    const [modalVisible, setModalVisible] = useState(false);

    function handleConfirm() {
        onConfirm(true);
        setModalVisible(false);
    }

    function handleCancel() {
        onCancel(true);
        setModalVisible(false);
    }
    
    useEffect(() => {
        open && setModalVisible(true)
    }, [open]);
    
    
    return <Container 
        animationType={animationType}
        transparent={transparent}
        visible={modalVisible}
        { ...rest }
    >
        <ContainerModal>
            <Card>
                <Content>
                    <Title>{title}</Title>
                    <Text>{description}</Text>
                </Content>
                <Footer>
                        <View>
                            <Button 
                                title={textBtnCancel} 
                                type={typeBtn1} 
                                onPress={handleCancel} />
                        </View>
                        <View>
                            <Button 
                                title={textBtnConfirm}
                                disabled={btnConfirm}
                                type={typeBtn2} 
                                onPress={handleConfirm} />
                        </View>
                </Footer>
            </Card>
        </ContainerModal>
    </Container>
}