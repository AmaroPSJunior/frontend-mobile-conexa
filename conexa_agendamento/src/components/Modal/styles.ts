import styled, { css } from 'styled-components/native';
import { Modal } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export type TypeProps = 'primary' | 'secondary';

export const Container = styled(Modal)``;

export const ContainerModal = styled(LinearGradient).attrs(({ theme }) => ({  
    colors: theme.COLORS.GRADIENT,
    start: { x: 0, y: 1},
    end: { x: 0.5, y: 0.5 }
}))`
    height: 100%;
`;

export const Content = styled.View`
    flex-direction: row;
    justify-content: space-between;
`;

export const Card = styled.View`
    top: 25%;
    margin: 32px;
    padding: 20px;
    padding-bottom: 0;
    border-radius: 12px;
    align-items: center;

    ${({ theme }) => css`
        font-family: ${theme.FONTS.TITLE};
        background-color: ${theme.COLORS.BACKGROUND};
    `};
`;

export const Title = styled.Text`
    font-size: 20px;
    text-align: center;
    padding: 20px 0;

    ${({ theme }) => css`
        color: ${theme.COLORS.PRIMARY_800};
        font-family: ${theme.FONTS.TEXT};
    `};
`;

export const Row = styled.View`
    flex-direction: row;
    justify-content: space-between;
`;

export const View = styled.View`
    width: 30%;
    margin-top: 15px;   
    flex-direction: row;
    justify-content: space-between;
`;

export const Text = styled.Text`

    ${({ theme }) => css`
        color: ${theme.COLORS.PRIMARY_800};
        font-family: ${theme.FONTS.TITLE};
    `};
`;

export const Footer = styled.View`
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
`;

export const Load = styled.ActivityIndicator.attrs(({ theme }) => ({
    color: theme.COLORS.TITLE
}))``;
