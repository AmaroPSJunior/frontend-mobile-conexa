import styled, { css } from 'styled-components/native';
import { LinearGradient } from 'expo-linear-gradient';

export const Title = styled.Text`
    font-size: 20px;
    padding: 10px 0;
    padding-right: 20px;
    margin-top: 20px;

    ${({ theme }) => css`
        color: ${theme.COLORS.TITLE};
        font-family: ${theme.FONTS.TITLE};
    `};
`;

export const SubTitle = styled.Text`
    font-size: 20px;
    padding: 0 32px;

    ${({ theme }) => css`
        color: ${theme.COLORS.TITLE};
        font-family: ${theme.FONTS.TITLE};
    `};
`;

export const Text = styled.Text`

    ${({ theme }) => css`
        color: ${theme.COLORS.PRIMARY_800};
        font-family: ${theme.FONTS.TITLE};
    `};
`;

export const Card = styled.View`
    margin: 10px 0;
    padding: 20px;
    width: 100%;
    border-radius: 12px;

    ${({ theme }) => css`
        font-family: ${theme.FONTS.TITLE};
        background-color: ${theme.COLORS.BACKGROUND};
    `};
`;

export const Row = styled.View`
    flex-direction: row;
    justify-content: space-between;
`;

export const View = styled.View`
    width: 30%;
    margin-top: 15px;   
    flex-direction: row;
    justify-content: space-between;
`;

export const Footer = styled(LinearGradient).attrs(({ theme }) => ({ 
    colors: theme.COLORS.GRADIENT_INVERSE,
}))`
    padding: 10px 32px;
    height: 75px;
`;

export const Load = styled.ActivityIndicator.attrs(({ theme }) => ({
    color: theme.COLORS.TITLE
}))`
    height: 400px;
`;

export const Header = styled(LinearGradient).attrs(({ theme }) => ({ 
    colors: theme.COLORS.GRADIENT,
}))`
    width: 100%;
    flex-direction: row;
    justify-content: space-between;
    padding: 30px;
    padding-top: 20px;
    padding-bottom: 20px;
    
`;

export const FooterNewSchedule = styled.View``;

export const Content = styled.ScrollView.attrs({
    showsVerticalScrollIndicator: false,
})`
    padding: 0 32px;
    
    ${({ theme }) => css`
        background-color: ${theme.COLORS.BACKGROUND_SECONDARY};
    `};
`;

export const Container = styled.View`
    flex: 1;
    justify-content: center;
`;