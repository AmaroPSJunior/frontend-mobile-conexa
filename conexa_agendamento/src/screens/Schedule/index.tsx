import React, { useEffect, useState } from 'react';

import { Detail } from '@components/Detail';
import { Button } from '@components/Button';
import { api } from '@services/api';
import { useNavigation, useRoute } from '@react-navigation/native';
import { ScheduleNavigationProps } from '@src/@types/navigation';
import { KeyboardAvoidingView, Platform } from 'react-native';
import { useAuth } from '@hooks/auth';
import { Modal } from '@components/Modal';

import {
    Container,
    Content,
    Footer,
    Title,
    Header,
    View
} from './styles';


export function Schedule() {
    const route = useRoute();
    const { id } = route.params as ScheduleNavigationProps;
    const { signOut } = useAuth();
    const [openModal, setOpenModal] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [schedule, setSchedule] = useState({});
    const navigation = useNavigation();
    
    async function detailSchedule(): Promise<void> {

        try {
            setIsLoading(true);
            const response = await api.get(`/consulta/${id}`);
            setSchedule(response.data.data);
        } catch (error) {
            console.log('error', error)
        } finally {
            setIsLoading(false);
        }
    }

    function handleNavigation() {
        navigation.navigate('home');
    }

    function onConfirm(val: boolean) {
        setOpenModal(false);
        signOut();
        navigation.navigate('signin');
    }
    
    function onCancel(val: boolean) {
        setOpenModal(false);
    }
    
    useEffect(() => {
        console.log('id', id)

        id && detailSchedule()
    }, [id])

    return (
        <Container>
            <KeyboardAvoidingView style={{flexGrow: 1}} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
            <Modal 
                transparent={false} 
                onConfirm={onConfirm}
                onCancel={onCancel}
                open={openModal}
            />  
                <Header>
                    <Title>{ 'Cadastro' }</Title>
                    <View>
                        <Button title='Sair' type='secondary' onPress={() => setOpenModal(true)} />
                    </View>    
                </Header>
                <Content>
                    <Detail schedule={schedule} />
                </Content>
                <Footer>
                    <Button
                        title='Voltar para listagem'
                        type='secondary'
                        onPress={handleNavigation}
                        isLoading={isLoading}
                    />
                </Footer>
            </KeyboardAvoidingView>
        </Container>
    )   
}
