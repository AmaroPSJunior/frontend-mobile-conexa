import React, { useState } from 'react';


import { Button } from '@components/Button';
import {
    ViewIDate,
    Row,
    Container,
    Content,
    Footer,
    Title,
    Header,
    View,
    Card,
    Text,
    Subtitle,
} from './styles';
import { Detail } from '@components/Detail';
import { api } from '@services/api';
import { KeyboardAvoidingView, Platform } from 'react-native';
import { useAuth } from '@hooks/auth';
import { useNavigation } from '@react-navigation/native';
import { Input } from '@components/Imput';
import { ISchedule } from '@src/@types/gobalTypes';
import { Modal } from '@components/Modal';

export function NewSchedule() {
    const [schedule, setSchedule] = useState<ISchedule | null>();
    const [isLoading, setIsLoading] = useState(false);
    const [showKeyboard, setShowKeyboard] = useState(false);
    const [openModal, setOpenModal] = useState(false);
    const [paciente, setPaciente] = useState('fulano da silva'); // deixe dados preenchido para facilitar na execução dos testes
    const [observacao, setObservacao] = useState('obs do agendamento de teste');
    const [dataConsulta, setDataConsulta] = useState('2022-03-14');
    const [horaConsulta, setHoraConsulta] = useState('08:43');
    const { signOut } = useAuth();
    const navigation = useNavigation();

    async function addNewSchedule() {
        try {
            setIsLoading(true);
            setShowKeyboard(false);
            const response = await api.post('/consulta', {
                idMedico: 1, // tive que chapar o dado pq não tem como recuperar via api de forma segura!
                paciente,
                observacao,
                dataConsulta: `${dataConsulta}T${horaConsulta}`,
            });
            setSchedule(response.data.data)
        } catch (error) {
            console.log('error', error)
        } finally {
            setIsLoading(false);
        }
    }

    async function handleSetSchedules() {
        addNewSchedule();
    }

    function handleNavigation() {
        navigation.navigate('home');
    }

    function onConfirm(val: boolean) {
        setOpenModal(false);
        signOut();
        navigation.navigate('signin');
    }

    function onCancel(val: boolean) {
        setOpenModal(false);
    }


    return (
        <Container>
            <Modal
                transparent={true}
                onConfirm={onConfirm}
                onCancel={onCancel}
                open={openModal}
            />
            <KeyboardAvoidingView style={{ flexGrow: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
                <Header>
                    <Title>{'Novo Agendamento'}</Title>
                    <View>
                        <Button title='Sair' type='secondary' onPress={() => setOpenModal(true)} />
                    </View>
                </Header>
                <Content>

                    {
                        schedule && <>
                            <Card>
                                <Subtitle>{'Agendamento realizado!'}</Subtitle>
                            </Card>
                            <Detail schedule={schedule} />
                        </>

                        || <>
                            <Input
                                placeholder="Nome do paciente"
                                type="secondary"
                                value={paciente}
                                onChangeText={setPaciente}
                            />

                            <Input
                                placeholder="Observacao"
                                type="secondary"
                                value={observacao}
                                onChangeText={setObservacao}
                            />

                            <Row>
                                <ViewIDate>
                                    <Input
                                        placeholder="Data da consulta"
                                        type="secondary"
                                        value={dataConsulta}
                                        onChangeText={setDataConsulta}
                                        showSoftInputOnFocus={showKeyboard}
                                    />
                                </ViewIDate>
                                <ViewIDate>
                                    <Input
                                        placeholder="Hora da consulta"
                                        type="secondary"
                                        value={horaConsulta}
                                        onChangeText={setHoraConsulta}
                                        showSoftInputOnFocus={showKeyboard}

                                    />
                                </ViewIDate>
                            </Row>
                        </>
                    }

                </Content>
                <Footer>
                    <Button
                        style={!schedule && { marginRight: 10 }}
                        title='Voltar'
                        type='secondary'
                        onPress={() => handleNavigation()}
                    />

                    {
                        !schedule && <Button
                            style={{ marginLeft: 10 }}
                            title='Salvar'
                            type='secondary'
                            onPress={handleSetSchedules}
                            isLoading={isLoading}
                        />
                    }

                </Footer>
            </KeyboardAvoidingView>
        </Container>
    )
}

