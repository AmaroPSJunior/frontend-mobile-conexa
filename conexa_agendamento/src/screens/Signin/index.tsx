import React, { useEffect, useState } from 'react';

import { Input } from '@components/Imput';
import { Container, Content, Title } from './styles';
import { Button } from '@components/Button';
import { KeyboardAvoidingView, Platform } from 'react-native';
import { useAuth } from '@hooks/auth';
import { useNavigation } from '@react-navigation/native';

export function SignIn() {
    const [email, setEmail] = useState('teste@email.com'); // deixe dados preenchido para facilitar na execução dos testes
    const [password, setPassword] = useState('123456');
    const { isLoading, user, signIn } = useAuth();
    const navigation = useNavigation();

    const handleSignIn = () => signIn(email, password);
    
    useEffect(() => {
        user && navigation.navigate('home');
    }, [user])
    

    return (
        <Container>
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
                <Content>
                    <Title>Entrar</Title>

                    <Input
                        placeholder="E-mail"
                        type="secondary"
                        value={email}
                        autoCorrect={false}
                        autoCapitalize="none"
                        onChangeText={setEmail}
                    />

                    <Input
                        placeholder="Senha" 
                        type="secondary" 
                        value={password}
                        secureTextEntry 
                        onChangeText={setPassword}
                    />

                    <Button
                        title='Entrar'
                        type='secondary'
                        isLoading={isLoading}
                        onPress={handleSignIn}
                    />
                </Content>
            </KeyboardAvoidingView>
        </Container>
    );
}
