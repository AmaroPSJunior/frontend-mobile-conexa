import styled, { css } from 'styled-components/native';
import { LinearGradient } from 'expo-linear-gradient';

export const Title = styled.Text`
    margin-bottom: 24px;
    align-self: flex-start;
    font-size: 32px;
    ${({ theme }) => css`
        color: ${theme.COLORS.TITLE};
        font-family: ${theme.FONTS.TITLE};
    `};
`;

export const Content = styled.ScrollView.attrs({
    showsVerticalScrollIndicator: false,
})`
    padding: 0 32px;
    width: 100%;
`;

export const Container = styled(LinearGradient).attrs(({ theme }) => ({ 
    colors: theme.COLORS.GRADIENT,
    start: { x: 0, y: 1},
    end: { x: 0.5, y: 0.5 }
}))`
    flex: 1;
    justify-content: center;
`;