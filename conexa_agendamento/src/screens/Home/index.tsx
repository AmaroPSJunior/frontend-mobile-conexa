import React, { useEffect, useState } from 'react';

import { KeyboardAvoidingView, Platform } from 'react-native';
import { useAuth } from '@hooks/auth';
import { useNavigation } from '@react-navigation/native';
import { api } from '@services/api';
import { ISchedule } from '@src/@types/gobalTypes';
import { Modal } from '@components/Modal';
import { Button } from '@components/Button';

import { 
    Container,
    Content,
    Footer,
    Title,
    SubTitle,
    Header,
    View,
    Text,
    Load,
    Card,
    Row 
} from './styles';

export function Home() {
    const { signOut, user } = useAuth();
    const navigation = useNavigation();
    const [schedules, setSchedules] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [openModal, setOpenModal] = useState(false);

    async function getSchedules() {
        try {
            setIsLoading(true);
            const response = await api.get('/consultas');
            setSchedules(response.data.data);
        } catch (error) {
            console.log('error', error);
        } finally {
            setIsLoading(false);
        }
    }
    
    function handleShowDetail(id: any) {
        navigation.navigate('schedule', { id });
    }
    
    const handleNavigation = () => {
        navigation.navigate('newschedule');
    };
    
    function onConfirm(val: boolean) {
        setOpenModal(false);
        signOut();
        navigation.navigate('signin');
    }
    
    function onCancel(val: boolean) {
        setOpenModal(false);
    }
    
    useEffect(() => {
        getSchedules();
    }, []);

    return (
        <Container>
            <Modal 
                transparent={false} 
                onConfirm={onConfirm}
                onCancel={onCancel}
                open={openModal}
            />  
            <KeyboardAvoidingView style={{flexGrow: 1}} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
                <Header>
                    <Title>{ 'Agendamento' }</Title>
                    <View>
                        <Button title='Sair' type='secondary' onPress={() => setOpenModal(true)} />
                    </View>
                </Header>
                <SubTitle>{`Olá ${user}, veja seus agendamentos: ` }</SubTitle>
                <Content>
                    { isLoading ? <Load /> : schedules.map((schedule: ISchedule, id) => (
                        <Card key={id} onTouchEndCapture={() => handleShowDetail(schedule.id)}>
                            <Row>
                                <Text>Paciente:</Text>
                                <Text>{ schedule.paciente }</Text>
                            </Row>
                            <Row>
                                <Text>Data:</Text>
                                <Text>{ schedule.dataConsulta }</Text>
                            </Row>
                        </Card>
                    ))}
                </Content>
                <Footer>
                    <Button
                        title='Novo Agendamento'
                        type='secondary'
                        onPress={() => handleNavigation()}
                    />
                </Footer>
            </KeyboardAvoidingView>
        </Container>
    )
}
