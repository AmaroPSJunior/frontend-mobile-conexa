import React, { createContext, ReactNode, useContext, useEffect, useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Alert } from 'react-native';
import { api } from '../services/api';

const USER_STORAGE = '@agendamento:user';
const TOKEN_STORAGE = '@agendamento:token';

interface ISchedule {
    dataConsulta?: string;
    id?: number;
    medico?: undefined | null | {
        email?: string;
        id?: number;
        nome?: string;
    };
    observacao?: string;
    paciente?: string;
}

type AuthContextData = {
    signIn: (email: string, password: string) => Promise<void>;
    signOut: () => Promise<void>;
    listSchedules: [] | null;
    schedule: {};
    isLoading: boolean;
    user: string;
}

type AuthProviderProps = {
    children: ReactNode;
}

export const AuthContext = createContext({} as AuthContextData);

function AuthProvider({ children }: AuthProviderProps) {
    const [isLoading, setIsLoading] = useState(true);
    const [user, setUser] = useState('');
    const [schedule, setSchedule] = useState({});
    const [listSchedules, setListSchedules] = useState(null);


    async function signIn(email: string, password: string) {
        try {
            setIsLoading(true);
            if (!email || !password) return Alert.alert('Lorin', 'Informe email e senha.');
            const authResponse = await api.post('/login', { "email": email, "senha": password });
            const { nome, token } = authResponse.data.data;

            api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
            setUser(nome);

            await AsyncStorage.setItem(USER_STORAGE, JSON.stringify(nome));
            await AsyncStorage.setItem(TOKEN_STORAGE, JSON.stringify(token));
        } catch (error) {
            console.log('error', error)
        } finally {
            setIsLoading(false);
        }

    }

    async function signOut() {
        setIsLoading(true);
        await AsyncStorage.removeItem(USER_STORAGE);
        await AsyncStorage.removeItem(TOKEN_STORAGE);
        delete api.defaults.headers.common.Authorization;
        setUser('');

        setIsLoading(false);
    }

    async function loadUserStorage() {
        try {
            const userStorage = await AsyncStorage.getItem(USER_STORAGE);
            const tokenStorage = await AsyncStorage.getItem(TOKEN_STORAGE);

            if (userStorage && tokenStorage) {
                setUser(JSON.parse(userStorage))
                api.defaults.headers.common['Authorization'] = `Bearer ${JSON.parse(tokenStorage)}`;
            }
        } catch (error) {
            console.log('error', error)
        } finally {
            setIsLoading(false);
        }
    }   
     

    useEffect(() => {
        loadUserStorage();
    }, [])


    return (
        <AuthContext.Provider value={{ 
            signIn,
            signOut,
            listSchedules,
            schedule, 
            isLoading,
            user
        }}>
            {children}
        </AuthContext.Provider>
    )
}

function useAuth() {
    const content = useContext(AuthContext)
    return content;
}

export { AuthProvider, useAuth };