import axios from 'axios';

export const api = axios.create({ 
    baseURL: 'http://desafio.conexasaude.com.br/api'
});